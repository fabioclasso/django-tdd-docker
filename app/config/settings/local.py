import os

from .base import *

DEBUG = os.environ.get("DJANGO_DEBUG", default=True)

SECRET_KEY = os.environ.get(
    "SECRET_KEY", default="'vl(&9@9)b%a5@sj&n6^4frkcw66((r)254+_z@k0sxa25ljh0b'"
)

INTERNAL_IPS = ["127.0.0.1", "0.0.0.0"]
